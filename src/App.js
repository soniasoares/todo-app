import React, {useState} from 'react';
import useLocalStorage from './hooks/useLocalStorage';
import TodosContext from './context/TodosContext';

import Header from './components/Header';
import TodosList from './components/TodosList';
import AddTodo from './components/AddTodo';
import EditTodo from './components/EditTodo';

function App() {
  const [todos, setTodos] = useLocalStorage('todos', []);
  const priorities = ['urgent', 'high', 'normal', 'low'];
  const [showForm, setShowForm] = useState(false);
  const [formID, setFormID] = useState('');
  const [selectedFilter, setSelectedFilter] = useState('all');
  const [showFiltersMenu, setShowFiltersMenu] = useState(false);

  return (
    <div className={`app-container ${showFiltersMenu ? 'nav-visible' : ''}`}>
      <TodosContext.Provider value={{
        todos, setTodos, 
        priorities, 
        setShowForm, 
        selectedFilter, setSelectedFilter,
        showFiltersMenu, setShowFiltersMenu}}>
        <Header />
        {(showForm==='add') && <AddTodo />}
        {(showForm==='edit') && <EditTodo id={formID} />}
        <TodosList setFormID={setFormID} />
      </TodosContext.Provider>
    </div>
  );
}

export default App;
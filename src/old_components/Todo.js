import React, { useState } from "react";
import './Todo.sass';

import { MaterialSymbol } from 'react-material-symbols';
import 'react-material-symbols/dist/rounded.css';

const TODOS_DEFAULT = [
    {
        todo: 'buy groceries',
        isCompleted: false
    },
    {
        todo: 'finish work',
        isCompleted: true
    },
    {
        todo: 'go for a walk',
        isCompleted: false
    },
];

const FADING_DUR = 300;

function TodosComponent() {
    const [newTodo, setNewTodo] = useState('');
    const [todos, setTodos] = useState([...TODOS_DEFAULT]);
    const [isFading, setIsFading] = useState([]);
    
    function createNewTodo(todo) {
        if(todo.length > 0) {
            setTodos([...todos, {
                todo: todo,
                isCompleted: false
            }]);
        }
    }

    function completeTodo(index) {
        const todosArray = [...todos];
        todosArray[index].isCompleted = !todosArray[index].isCompleted;
        setTodos(todosArray);
    }

    function deleteTodo(index) {
        setIsFading([index]);
        setTimeout(() => {
            let todosArray = [...todos];
            todosArray.splice(index, 1);
            setTodos(todosArray);
            setIsFading([]);
        }, FADING_DUR);
    }

    function sortByCompleted() {
        setTodos([
            ...todos.filter(todo => !todo.isCompleted),
            ...todos.filter(todo => todo.isCompleted)
        ]);
    }

    function deleteAllCompleted() {
        setIsFading([...todos.reduce((r, todo, index) => todo.isCompleted ? (r.push(index), r) : r , [])]);
        setTimeout(() => {
            setTodos(todos.filter(todo => !todo.isCompleted));
            setIsFading([]);
        }, FADING_DUR);
        
    }
    
    return (
        <div className="todo-module">
            <input className="todo-input" 
                type="text" 
                placeholder="What needs to get done?"
                value={newTodo}
                onChange={e => setNewTodo(e.target.value)}
                onKeyDown={e => {
                    if(e.key === 'Enter') {
                        createNewTodo(newTodo);
                        setNewTodo('');
                    }
                }} />
            <p className="todo-info">Tasks To Complete: {todos.filter(todo => !todo.isCompleted).length}</p>
            <ul className="todo-list">
                {todos.map((todo,index) => (
                    <li className={isFading.includes(index) ? 'todo fading' : 'todo'} key={index}>
                        <div className="check" onClick={() => completeTodo(index)}>{todo.isCompleted && <MaterialSymbol icon="check" />}</div>
                        <div className={todo.isCompleted ? 'label done' : 'label'}>{todo.todo}</div>
                        <div className="delete" onClick={() => deleteTodo(index)}>
                            <MaterialSymbol icon="delete" />
                        </div>
                    </li>
                ))}
            </ul>

            <div className="todo-actions">
                <button onClick={sortByCompleted}><MaterialSymbol icon="sort" /> Sort by Completed</button>
                <button onClick={deleteAllCompleted}><MaterialSymbol icon="delete" /> Delete all Completed</button>
            </div>
        </div>
    );
}

export default TodosComponent;
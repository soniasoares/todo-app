import { useState, useEffect } from 'react';

const useLocalStorage = (key, initialValue) => {

  // lazy initialization
  // function will be executed only once,
  // even if the hook is called multiple times on every re-render
  const [value, setValue] = useState(() => {

    // we're checking to see if there is any value in local storage 
    // with the provided key and then we return the value
    try {
      const localValue = window.localStorage.getItem(key);
      return localValue ? JSON.parse(localValue) : initialValue;
    } catch (error) {
      return initialValue;
    }
  });

  // if there is any change in the key or value, 
  // we'll update the local storage
  useEffect(() => {
    window.localStorage.setItem(key, JSON.stringify(value));
  }, [key, value]);

  return [value, setValue];
};

export default useLocalStorage;
import moment from 'moment';

// Date utils
export const isDateOverdue = (date) => {
  return moment(date).format('YYYYMMDD') < moment().format('YYYYMMDD');
};
export const isDateDueToday = (date) => {
  return moment(date).format('YYYYMMDD') === moment().format('YYYYMMDD') ? true : false;
}

// List Utils
export const listFilterUtils = {
  completedItems: (list) => {
    return list.filter(x => x.completed);
  },
  pendingItems: (list) => {
    return list.filter(x => !x.completed);
  },
  dueTodayItems: (list) => {
    return list.filter(x => isDateDueToday(x.dueDate));
  },
  overdueItems: (list) => {
    return list.filter(x => isDateOverdue(x.dueDate) && !x.completed);
  }
}
import React, { useState, useContext } from 'react';
import TodosContext from '../context/TodosContext';

import { v4 as uuidv4 } from 'uuid';
import { MaterialSymbol } from 'react-material-symbols';
import 'react-material-symbols/dist/rounded.css';

const TodosForm = (props) => {
  const {showForm, setShowForm} = useContext(TodosContext);
  const [todo, setTodo] = useState(() => {
    return {
      name: props.todo ? props.todo.name : '',
      dueDate: props.todo ? props.todo.dueDate : '',
      priority: props.todo ? props.todo.priority : 'normal',
    };
  });
  const { name, dueDate, priority } = todo;
  const [errorMsg, setErrorMsg] = useState('');

  const handleCancel = () => {
    setShowForm(false);
  };

  const handleSubmit = (event) => {
    event.preventDefault(); 
    
    if (!todo.name || todo.name.trim() === "") {
      setErrorMsg("Enter valid message");
      return;
    }
   
    const newTodo = {
      id: uuidv4(),
      name,
      dueDate,
      priority,
      completed: false
    };
    props.handleSubmit(newTodo);
  };

  const handleInputChange = (event) => {
    const { name, value } = event.target;
    setTodo((prevState) => ({
      ...prevState,
      [name]: value
    }));
  };

  return (
    <div className="todo-form">
      <form onSubmit={handleSubmit}>
        <header className='form-header'>
          <h2>{props.editForm ? 'Edit': 'Add'} Form</h2>
          <div className="close-btn" onClick={handleCancel}>
            <MaterialSymbol icon='close' />
          </div>
          {errorMsg && <div className='error-msg'>{ errorMsg }</div> }
        </header>

        <InputField 
          placeholder="What needs to be done?"
          type="text"
          name="name" 
          value={name} 
          autoFocus={true} 
          onChange={handleInputChange} />
        <InputField 
          label="Due Date"
          type="date"
          name="dueDate" 
          value={dueDate} 
          onChange={handleInputChange} />
        <PrioritiesField value={priority} handleInputChange={handleInputChange} />
        <div className="submit">
          <div className="btn cancel-btn" onClick={handleCancel}>
            <MaterialSymbol icon='cancel' /> Cancel
          </div>
          <button className="btn btn-primary">
            <MaterialSymbol icon={props.editForm ? 'edit': 'add'} /> Submit
          </button>
        </div>
      </form>
    </div>
  );
};

const PrioritiesField = ({value, handleInputChange}) => {
  const {priorities} = useContext(TodosContext);

  return (
    <div className="priorities-field">
      <label>Priority</label>
      <ul>
        {priorities.map((priority, index) => (
          <li>
            <label key={index} className={`priority-field ${priority}`}>
              <input 
                name="priority" 
                value={priority} 
                type="radio" 
                checked={value === priority ? 'checked' : ''} 
                onChange={handleInputChange} /> 
                <span className='input'></span>
                <span className='label'>{priority}</span>
            </label>
          </li>
        ))}
      </ul>
    </div>
  )
};

const InputField = ({
  label, 
  placeholder,
  type,
  name,
  value,
  onChange,
  autoFocus
}) => {
  return (
    <div className={`field field-${name}`}>
      {label && <label>{label}</label>}
      <input 
        placeholder={placeholder} 
        type={type} 
        name={name} 
        value={value} 
        autoFocus={autoFocus}
        onChange={onChange} />
    </div>
  );
};

export default TodosForm;
import React, { useContext, useEffect, useState } from 'react';
import TodosContext from '../context/TodosContext';
import TodoFormBtn from './TodoFormBtn';
import {isDateDueToday, listFilterUtils} from '../Utils';

import { MaterialSymbol } from 'react-material-symbols';
import 'react-material-symbols/dist/rounded.css';


const TodosList = ({setFormID}) => {
  const {priorities, todos, setTodos, selectedFilter} = useContext(TodosContext);
  const [filteredTodos, setFilteredTodos] = useState([...todos]);
  const [isFading, setIsFading] = useState([]);
  const FADING_DUR = 300;

  // updated filters
  useEffect(() => {

    // sort by priority
    let sortedTodos = [...todos].sort((a,b) => priorities.indexOf(a.priority) - priorities.indexOf(b.priority));

    switch (selectedFilter) {
      case 'completed':
        setFilteredTodos(listFilterUtils.completedItems(sortedTodos));
        break;
      case 'pending':
        setFilteredTodos(listFilterUtils.pendingItems(sortedTodos));
        break;
      case 'due today':
        setFilteredTodos(listFilterUtils.dueTodayItems(sortedTodos));
        break;
      case 'overdue':
        setFilteredTodos(listFilterUtils.overdueItems(sortedTodos));
        break;
      default:
        setFilteredTodos([...sortedTodos]);
        return;
    }
  }, [selectedFilter, todos, setFilteredTodos, priorities]);

  const handleDelete = (id) => {
    setIsFading([id]);
    setTimeout(() => {
      setTodos(todos.filter(todo => todo.id !== id));
      setIsFading([]);
    }, FADING_DUR);
  };

  const handleComplete = (id) => {
    const todoIndex = todos.findIndex(x => x.id === id);
    const todosArray = [...todos];
    todosArray[todoIndex].completed = !todosArray[todoIndex].completed;
    setTodos(todosArray);
  }

  return (
    <ul className="todos-list">
      {filteredTodos.map(todo => (
        <li key={todo.id} className={`todo-item ${todo.completed ? 'completed': ''} ${isDateDueToday(todo.dueDate) ? 'due-today' : ''} ${todo.priority} ${isFading.includes(todo.id) ? 'fading' : ''}`}>

          <div className="todo-wrapper" onClick={() => handleComplete(todo.id)}>
            <div className="priority"></div>
            <div className='completed-check'>
              <MaterialSymbol icon='check' />
            </div>
            <div className='description'>
              <div className='name'>{todo.name}</div>
              <div className='date'>{todo.dueDate}</div>
            </div>
          </div>

          <div className='actions'>
            <button className='btn btn-delete' onClick={() => handleDelete(todo.id)}>
              <MaterialSymbol icon='delete' />
            </button>
            <TodoFormBtn 
              name='edit'
              icon='edit'
              formID={todo.id}
              setFormID={setFormID} />
          </div>
        </li>
      ))}
    </ul>
  );
};


export default TodosList;
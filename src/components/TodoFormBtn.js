import { useContext } from 'react';
import TodosContext from '../context/TodosContext';

import { MaterialSymbol } from 'react-material-symbols';
import 'react-material-symbols/dist/rounded.css';

const TodoFormBtn = ({name, label, icon, formID, setFormID}) => {
  const {setShowForm} = useContext(TodosContext);

  const handleClick = () => {
    setShowForm(name);
    if(formID) {
      setFormID(formID);
    }
  };

  return (
    <button className={`btn btn-${name}`} onClick={handleClick}>
      <MaterialSymbol icon={icon} />
      {label && <span className="label">{label}</span>}
    </button>
  )
};

export default TodoFormBtn;
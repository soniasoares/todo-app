import React, { useContext} from 'react';
import TodosContext from '../context/TodosContext';
import {listFilterUtils} from '../Utils';

import { MaterialSymbol } from 'react-material-symbols';
import 'react-material-symbols/dist/rounded.css';

const Filters = () => {
  const {showFiltersMenu, selectedFilter, setSelectedFilter} = useContext(TodosContext);
  const filters = ['all', 'pending', 'due today', 'overdue', 'completed'];

  return (
    <>
      <div className="filters-nav-actions">
        <FilterItem isSelected={true} filter={selectedFilter} />
        <FiltersNavBtn />
      </div>
      <nav className={`filters-nav ${showFiltersMenu ? 'visible' : ''}`}>
        <div className="filters-menu">
          {filters.map((filter, index) => (
            <FilterItem key={index} filter={filter} selectedFilter={selectedFilter} setSelectedFilter={setSelectedFilter} />
          ))}
        </div>
      </nav>
    </>
  );
};

const FiltersNavBtn = () => {
  const {showFiltersMenu, setShowFiltersMenu} = useContext(TodosContext);

  const handleClick = () => {
    setShowFiltersMenu(!showFiltersMenu);
  };

  return (
    <div className='filters-nav-btn' onClick={handleClick}>
      <MaterialSymbol icon={showFiltersMenu ? 'close': 'menu'} />
    </div>
  );
};

const FilterItem = ({isSelected, filter, selectedFilter, setSelectedFilter}) => {
  const {todos} = useContext(TodosContext);

  const handleFilter = (filter) => {
    setSelectedFilter(filter);
  };

  const setQuantity = (filter) => {
    switch (filter) {
      case 'completed':
        return listFilterUtils.completedItems(todos).length;
      case 'pending':
        return listFilterUtils.pendingItems(todos).length;
      case 'due today':
        return listFilterUtils.dueTodayItems(todos).length;
      case 'overdue':
        return listFilterUtils.overdueItems(todos).length;
      default:
        return todos.length;
    }
  };

  return (
    <div 
      className={`${isSelected ? 'selected-filter' : 'filter'} ${(!isSelected && selectedFilter === filter) ? 'active' : ''}`} 
      onClick={() => (!isSelected) ? handleFilter(filter) : false}>
        {!isSelected && <MaterialSymbol icon="folder" />}
        <span className='label'>{filter}</span> 
        <span className='quantity'>{setQuantity(filter)}</span>
    </div>      
  );
};

export default Filters;
import React, { useContext } from 'react';
import TodosContext from '../context/TodosContext';
import TodoForm from './TodoForm';

const EditTodo = ({id}) => {
  const { todos, setTodos, setShowForm } = useContext(TodosContext);
  const todoToEdit = todos.find(todo => todo.id === id);

  const handleSubmit = (todo) => {
    const filteredTodos = todos.filter(todo => todo.id !== id);
    setTodos([todo, ...filteredTodos]);
    setShowForm(false);
  };

  return (
    <React.Fragment>
      <TodoForm todo={todoToEdit} handleSubmit={handleSubmit} editForm={true} />
    </React.Fragment>
  );
};

export default EditTodo;
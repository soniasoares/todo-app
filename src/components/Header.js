import React, { useEffect, useState } from "react";
import moment from 'moment';
import TodoFormBtn from './TodoFormBtn';
import Filters from './Filters';

const Header = () => {
  return (
    <header className="app-header">
      <Datetime />
      <TodoFormBtn 
        name='add'
        label='Add Todo' 
        icon='add' />
      <Filters />
    </header>
  );
};

const Datetime = () => {
  const [date,setDate] = useState(new Date());

  useEffect(() => {
    var timer = setInterval(() => setDate(new Date()), 1000 );
    return function cleanup() {
      clearInterval(timer);
    }
  });

  return (
    <div className="datetime">
      <div className="datetime-time">{moment(date).format('LT')}</div>
      <div className="datetime-date">{moment(date).format('ddd, ll')}</div>
    </div>
  );
};

export default Header;
import React, { useContext } from 'react';
import TodosContext from '../context/TodosContext';
import TodoForm from './TodoForm';

const AddTodo = () => {
  const {todos, setTodos, setShowForm} = useContext(TodosContext);

  const handleSubmit = (todo) => {
    setTodos([todo, ...todos]);
    setShowForm(false);
  }

  return (
    <React.Fragment>
      <TodoForm handleSubmit={handleSubmit} />
    </React.Fragment>
  );
};

export default AddTodo;